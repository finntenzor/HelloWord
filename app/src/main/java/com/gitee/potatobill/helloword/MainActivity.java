package com.gitee.potatobill.helloword;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.util.Random;

public class MainActivity extends Activity 
{
    public final static double rate = 0.6;
    public final static boolean flag = true; //显示翻译
    private static String[] rawWords = Const.UNIT3_1;
    private static Word[] words;
    
    static {
        words = new Word[rawWords.length];
        for (int i = 0; i < words.length; i++) {
            words[i] = new Word(rawWords[i]);
        }
    }
    
    TextView tvSpeed;
    TextView tvWord;
    TextView tvTrans;
    EditText etInput;
    Word temp;
    int times;
    long time;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        tvSpeed = (TextView) findViewById(R.id.mainTextViewSpeed);
        tvWord = (TextView) findViewById(R.id.mainTextViewWord);
        tvTrans = (TextView) findViewById(R.id.mainTextViewTrans);
        etInput = (EditText) findViewById(R.id.mainEditTextInput);
        etInput.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence p1, int p2, int p3, int p4)
                {
                    // Nothing
                }

                @Override
                public void onTextChanged(CharSequence p1, int p2, int p3, int p4)
                {
                    // Nothing
                }

                @Override
                public void afterTextChanged(Editable p1)
                {
                    if (temp.word.equals(p1.toString())) {
                        if (!flag) {
                            Toast.makeText(MainActivity.this, temp.trans, Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MainActivity.this, "赞", Toast.LENGTH_SHORT).show();
                        }
                        nextWord();
                    }
                }
        });
        time = 0;
        tvTrans.setText("");
        nextWord();
    }
    
    private void init() {
        
    }
    
    private void nextWord() {
        Random r = new Random();
        int index = r.nextInt(words.length);
        temp = words[index];
        int len = temp.word.length();
        char[] fin = temp.word.toCharArray();
        while (temp.word.equals(new String(fin))) {
            for (int i = 0; i < len; i++) {
                if (r.nextDouble() > rate) fin[i] = '_';
            }
        }
        tvWord.setText(fin, 0, len);
        if (flag) tvTrans.setText(temp.trans);
        etInput.setText("");
        if (time == 0) {
            time = System.currentTimeMillis();
            tvSpeed.setText("");
            times = 0;
        } else {
            times++;
            double speed = ((double) times) / ((System.currentTimeMillis() - time) / 60000.0);
            double spd = ((int) (100 * speed)) / 100.0;
            tvSpeed.setText("手速" + spd + "词/分钟");
        }
    }
    
    private static class Word {
        public final static String at = "@";
        String word;
        String trans;
        Word(String s) {
            String[] temp = s.split(at);
            if (temp.length == 2) {
                this.word = temp[0];
                this.trans = temp[1];
            } else {
                throw new IllegalArgumentException("Not an argument word");
            }
        }
    }
}
