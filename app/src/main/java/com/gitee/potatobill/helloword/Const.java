package com.gitee.potatobill.helloword;

public class Const {
    public final static String[] UNIT1_2 = {
        "sophomore@二年级学生",
        "ironic@讽刺的",
        "gender@性别",
        "colonialism@殖民主义",
        "modernism@现代主义",
        "industrial@工业的",
        "overthrow@颠覆",
        "repressive@残酷的",
        "rebel@反抗",
        "chant@反复地喊",
        "destruction@摧毁",
        "petition@请愿书",
        "bracelet@手镯",
        "inspirational@鼓舞人心的",
        "philosophy@思想体系",
        "assert@自信地说;断言",
        "collectively@共同地",
        "nostalgic@怀旧的",
        "frustration@挫折",
        "takeover@接管",
        "invade@侵略",
        "hostage@人质",
        "resonance@共鸣反响",
        "era@时代",
        "technological@技术的",
        "profound@巨大的",
    };
    
    public final static String[] UNIT3_1 = {
        "teammate@队友",
        "collide@碰撞",
        "tournament@锦标赛",
        "clue@线索",
        "pitch@场地",
        "madly@猛烈的",
        "sheer@纯粹的",
        "hum@哼",
        "mournful@悲伤的",
        "reminiscent@怀旧的"
    };
    
    public final static String[] UNIT3_2_2= {
        "championship@锦标赛",
        "flutter@挥动",
        "unified@联合的",
        "incur@招致",
        "vocal@直言不讳的，嗓音的",
        "performer@表演者",
        "strain@压力",
        "eclipse@使黯然失色"
    };
}
